// SPDX-FileCopyrightText: 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import ucFirst from '../lib/ucFirst.mjs';
import lcFirst from '../lib/lcFirst.mjs';

tap.equal(ucFirst('hello'), "Hello", 'Uppercased the first letter');
tap.equal(lcFirst('HELLO'), "hELLO", 'Lowercased the first letter');
