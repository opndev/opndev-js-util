// SPDX-FileCopyrightText: 2021 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import defined from '../lib/defined.mjs';

tap.equal(defined("what"), true, '"what" is defined');
tap.equal(defined([]), true, '.. and so is an array');
tap.equal(defined({}), true, '.. objects too');

let foo;
tap.equal(defined(null), false, "`null` isn't defined");
tap.equal(defined(foo), false, ".. and so is `let foo;`");
