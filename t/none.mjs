// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import none from '../lib/none.mjs';

const haystack = new Array('foo', 'bar', 'baz', 1, 2, 3);

tap.equal(none("bas", haystack), true, "We cannot find our needle");
tap.equal(none("foo", haystack), false, ".. and now our needle is found")

tap.equal(none(4, haystack), true, "We couldnt find our needle");
tap.equal(none(1, haystack), false, ".. and now our needle is found")

function check_if_none(i) {
   return i === "bas";
}

tap.equal(none(check_if_none, haystack), true,
     "We couldnt find our needle via a function")

function check_if_found(i) {
   return i === "foo";
}
tap.equal(none(check_if_found, haystack), false, ".. and now our needle is found");

tap.equal(none(element => element === 'bas', haystack), true,
   "Needle wasnt found in arrow function");
tap.equal(none(element => element === 'baz', haystack), false, "... and now it is");
