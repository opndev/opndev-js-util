// SPDX-FileCopyrightText: 2021 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 * Check if a variable is defined
 * @function defined
 * @param {*} what Anything really
 * @returns {Boolean} true if defined, false otherwise
 */

export default function defined(what) {
  if (what === null)
    return false

  if (typeof what === 'undefined')
    return false

  /* This will probably never be hit:
   * https://stackoverflow.com/questions/4725603/variable-undefined-vs-typeof-variable-undefined
   */
  if (what === undefined)
    return false

  return true
}

export { defined };

