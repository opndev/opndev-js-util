// SPDX-FileCopyrightText: 2021 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 * Check if a value is preset in the given array, can be done via a function or
 * a primitive. Objects tend to be harder. Use lodash instead.
 *
 * @function any
 * @param {*} needle Anything
 * @param {Array} haystack Anything
 * @returns {Boolean} true if needle is found, false otherwise
 */
export default function any(needle, haystack) {

  if (!Array.isArray(haystack))
    throw "haystack is not an array";

  if (typeof needle === 'function')
    return haystack.some(needle)

  return haystack.includes(needle)
}

export { any }
