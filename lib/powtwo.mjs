// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 * Returns the power of two
 * @function pow_of_two
 * @param {*} what an integer
 * @returns {Integer} the power of two
 *
 * @example
 * // Based on the amount of players you can extract how many rounds a
 * tournament has (in single elimination
 *
 * const rounds = pow_of_two(players)
 */

export default function pow_of_two(i) {
  let parsed = Number.parseInt(i, 10);
  if (Number.isNaN(parsed))
    throw new Error(`${i} is not a number`);

  let count = 0;
  while (parsed != 1) {
    parsed = parsed >> 1;
    count++;
  }
  return count;
}

export { pow_of_two };

