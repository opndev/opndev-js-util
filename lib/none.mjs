// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 * Check if a value is not preset in the given array, can be done via a
 * function or a primitive.
 *
 * @function none
 * @param {*} needle Anything
 * @param {Array} haystack Anything
 * @returns {Boolean} true if needle isnt found, false otherwise
 */

export default function none(needle, haystack) {
  if (!Array.isArray(haystack))
    throw "haystack is not an array";

  if (typeof needle === 'function')
    return haystack.some(needle) ? false : true

  return haystack.includes(needle) ? false : true
}

export { none }
