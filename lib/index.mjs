// SPDX-FileCopyrightText: 2021 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 *
 * @module module:@opndev/utils
 *
 * @exports any
 * @exports none
 * @exports defined
 * @exports pow_of_two
 * @exports is_pow_of_two
 *
 * Export all the useful functions. Unfortunatly, without webpack's
 * require.context we cannot automaticly load everything. So we just keep
 * typing. ES6 requires strict syntax on import and exports :/
 */
export { any } from './any.mjs'
export { none } from './none.mjs'
export { defined } from './defined.mjs'
export { pow_of_two } from './powtwo.mjs'
export { is_pow_of_two } from './ispowtwo.mjs'
export { range } from './range.mjs'
export { ucFirst } from './ucFirst.mjs'
export { lcFirst } from './lcFirst.mjs'
