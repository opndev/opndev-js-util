// SPDX-FileCopyrightText: 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 * Check if a value is preset in the given array, can be done via a function or
 * a primitive. Objects tend to be harder. Use lodash instead.
 *
 * @function lcFirst
 * @param {String} string Anything
 * @returns {String} string With the first letter uppercased
 */
export default function lcFirst(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

export { lcFirst }
