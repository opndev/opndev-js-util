// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 * Check if something is the power of two
 * @function is_pow_of_two
 * @param {Integer} what The number you want to validate as the power of two
 * @returns {Boolean} True is something is the power of two
 *
 * @example
 * // A tournamen draw has to be a power of two
 * if (is_pow_of_two(draw))
 *  throw "A draw must be a power of two"
 */

export default function is_pow_of_two(i) {
  const parsed = Number.parseInt(i, 10);
  if (Number.isNaN(parsed))
    throw new Error(`${i} is not a number`);

  return (!(parsed & (parsed-1)) && parsed) ? true : false;

}

export { is_pow_of_two };

